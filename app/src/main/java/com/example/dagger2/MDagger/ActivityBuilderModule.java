package com.example.dagger2.MDagger;


import com.example.dagger2.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {
    @ContributesAndroidInjector()
    abstract MainActivity mainActivity();


}
