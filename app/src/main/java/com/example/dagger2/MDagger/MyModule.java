package com.example.dagger2.MDagger;

import dagger.Module;
import dagger.Provides;

@Module
public  class MyModule {

    @Provides
    String getTitle(){
        return "ModuleTitle";
    }


}
